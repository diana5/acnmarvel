import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Heroe } from '../classes/heroe';

@Injectable({
  providedIn: 'root'
})
export class HeroesService {

  private protocol = 'https:';
  private ApiUrl = '//gateway.marvel.com:443/v1/public/';
  public heroes: Array<Heroe> = [];
  public heroe: Heroe;
  page: number;
  step = 20;
  total = 0;


  public group_colors = {"azul" : "#1f8ff7",
                        "violeta":"#a43de3",
                        "naranjo":"#df5c0f",
                        "verde":"#0ea521"}
  
  public teams = new Map();

  constructor(private http: HttpClient) { }

  resetPager() {
    
}

getHeroes (nameStartsWith?: string, page?: number){
  this.heroes = [];
  console.log("TEAMS");
  console.log(Array.from(this.teams));
  if (page || page === 0) {
    this.page = page;
  }
  const url = this.protocol + this.ApiUrl + 'characters?apikey=56d2cc44b1c84eb7c6c9673565a9eb4b'
  + '&offset=' + (this.page * this.step)
  + (nameStartsWith ? ('&nameStartsWith=' + nameStartsWith) : '');
  return this.http.get<any>(url)
  


}

getHeroe(id) {
  const url = this.protocol + this.ApiUrl + 'characters/' + id + '?apikey=56d2cc44b1c84eb7c6c9673565a9eb4b';
  return this.http.get<any>(url);
}

getTeamColor(id):string{
  if(this.teams.get(id)!=undefined){
  return this.teams.get(id);
  }
  else{
  return "";
  }
}
 
}
