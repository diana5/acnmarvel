import { HttpClientModule } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { Observable } from 'rxjs';

import { HeroesService } from './heroes.service';

describe('HeroesService', () => {
  let service: HeroesService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations:[

      ],
      imports:[
        HttpClientModule,
      ]
    });
    service = TestBed.inject(HeroesService);
    service = new HeroesService(null);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  // it('#getValue should return real value', () => {
  //   expect(service.getHeroes()).toBe(new Observable);
  // });
});
