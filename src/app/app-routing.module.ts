import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HeroProfileComponent } from './components/hero-profile/hero-profile.component';
import { HeroesListComponent } from './components/heroes-list/heroes-list.component';

const routes: Routes = [
  { path: 'lista-heroes', component: HeroesListComponent},
  { path: 'heroe/:id', component: HeroProfileComponent},
  { path: '**', redirectTo: 'lista-heroes'}
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})


export class AppRoutingModule { 

}
