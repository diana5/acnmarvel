import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA,NO_ERRORS_SCHEMA  } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeroesListComponent } from './components/heroes-list/heroes-list.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { HeroProfileComponent } from './components/hero-profile/hero-profile.component';
import { ModalPollComponent } from './components/modal-poll/modal-poll.component';
import {RouterTestingModule} from '@angular/router/testing';
import { ComponentFixtureAutoDetect } from '@angular/core/testing';
import { HeroesService } from './services/heroes.service';

@NgModule({
  declarations: [
    AppComponent,
    HeroesListComponent,
    HeroProfileComponent,
    ModalPollComponent,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    RouterTestingModule,
    

  ],
  providers: [ { provide: ComponentFixtureAutoDetect, useValue: true },
    // { provide: HeroesService, useClass: HeroServiceMock }
  ],
  bootstrap: [AppComponent],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA,NO_ERRORS_SCHEMA],

})
export class AppModule { }
