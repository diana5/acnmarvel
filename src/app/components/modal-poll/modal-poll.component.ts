import { Component, OnInit, Input, Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'app-modal-poll',
  templateUrl: './modal-poll.component.html',
  styleUrls: ['./modal-poll.component.css']
})
export class ModalPollComponent implements OnInit {
 // public show_modal: boolean = false;
  public show_modal: boolean = true;
  @Input() public title_modal : string;
  @Input() public team_selected : string;

  @Output() setTeam:EventEmitter<string> = new EventEmitter<string>();


  constructor() { }

  ngOnInit() {
  }

  toggle_modal(): void {
    this.show_modal = !this.show_modal;
    
  }
  

  send_team(team: string): void {
    this.setTeam.emit(team);
    this.toggle_modal();
  }

}
