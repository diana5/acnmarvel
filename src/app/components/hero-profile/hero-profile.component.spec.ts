import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { ActivatedRoute, Router, Routes } from '@angular/router';
import { of } from 'rxjs';
import { HeroesService } from 'src/app/services/heroes.service';
import { HeroesListComponent } from '../heroes-list/heroes-list.component';

import { HeroProfileComponent } from './hero-profile.component';


describe('HeroProfileComponent', () => {

  const HEROE_OBJECT ={
    id:'1',
    name:'Spiderman',
    description: 'El hombre que araña',
    modified:new Date(1518417160),
    thumbnail:
    {
    'path': 'https://i.pinimg.com/originals/c2/93/56/c293563aa553250601d8cb768c044d4b',
    'extension': 'jpg'
    },
    resourceURI:'http://gateway.marvel.com/v1/public/characters/1011334',
    teamColor:'yellow'};

  let component: HeroProfileComponent;
  let fixture: ComponentFixture<HeroProfileComponent>;
  let heroesService: HeroesService;


  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HeroProfileComponent ],
      providers: [ActivatedRoute]
    })
    .compileComponents();
  });

  it('should be created', () => {
    expect(HEROE_OBJECT).toBeDefined();
  });
});


describe('HeroProfileComponent', () => {


  //mock de routing
  
  const routes: Routes = [
    { path: 'lista-heroes', component: HeroesListComponent},
    { path: 'heroe/1', component: HeroProfileComponent},
    { path: '**', redirectTo: 'lista-heroes'}
    
  ]; 

  let component: HeroProfileComponent;
  let fixture: ComponentFixture<HeroProfileComponent>;
  let activatedRoute:ActivatedRoute;

//   beforeEach(async () => {
//     await TestBed.configureTestingModule({
//       declarations: [ HeroProfileComponent ],
//       providers: [ActivatedRoute]
//     })
//     .compileComponents();
    
//   });

//   let router = TestBed.get(Router); 
//   location = TestBed.get(Location); 
//   it('navigate to "" redirects you to /lista-heroes', fakeAsync(() => { 
//     router.navigate(['']); 
//     tick(); 
//     expect(component.goBack()).toBeTrue(); 
  
// }));


 
// beforeEach(() => {
//   TestBed.configureTestingModule({
//     declarations: [
//       HelloComponent
//     ],
//     providers: [
//       {provide: UserService, useClass: MockUser}
//     ]
//   });
//   component = TestBed.createComponent(HelloComponent).componentInstance;
//   userService = TestBed.get(UserService);
// });
});
