import { HttpClient, HttpClientModule } from '@angular/common/http';
import { Location, LocationStrategy, PathLocationStrategy } from '@angular/common';
import { ComponentFixture, TestBed,inject, fakeAsync, tick  } from '@angular/core/testing';
import { ActivatedRoute, Router, Routes } from '@angular/router';
import { Observable, of } from 'rxjs';
import { Heroe } from 'src/app/classes/heroe';
import { HeroesService } from 'src/app/services/heroes.service';

import { HeroesListComponent } from './heroes-list.component';
import { RouterTestingModule } from '@angular/router/testing';
import { HeroProfileComponent } from '../hero-profile/hero-profile.component';
import { AppComponent } from 'src/app/app.component';

export const listHeroes: Heroe[]  =[{
  id:'1',
  name:'Spiderman',
  description: 'El hombre que araña',
  modified:new Date(1518417160),
  thumbnail:
  {
  path: 'https://i.pinimg.com/originals/c2/93/56/c293563aa553250601d8cb768c044d4b',
  extension: 'jpg'
  },
  resourceURI:'http://gateway.marvel.com/v1/public/characters/1011334',
  teamColor:'yellow'}];

//mock para el service
export class MockHeroesService {
  public getHeroes(nameStartsWith:string,page:string):Observable<Heroe[]>{
    return of(listHeroes);
  }
  private protocol = 'https:';
  private ApiUrl = '//gateway.marvel.com:443/v1/public/';
  public heroes: Array<Heroe> = [];
  public heroe: Heroe;
  page= 2;
  step = 20;
  total = 0;
  }

  //mock para el submitSearch
  export class MockSubmitSearch{
 public submitSearch(){
  
 }
  }

  //mock de routing
  
  const routes: Routes = [
    { path: 'lista-heroes', component: HeroesListComponent},
    { path: 'heroe/1', component: HeroProfileComponent},
    { path: '**', redirectTo: 'lista-heroes'}
    
  ]; 

describe('HeroesListComponent', () => {

  let component: HeroesListComponent;
  let fixture: ComponentFixture<HeroesListComponent>;

 
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HeroesListComponent,AppComponent ],
      imports:[
        HttpClientModule,
        RouterTestingModule.withRoutes(routes)
        
      ],
      
      providers: [{provide:HeroesService, useClass:MockHeroesService},
                   Location,
                  {provide: LocationStrategy, useClass: PathLocationStrategy}
]
      
    })
    .compileComponents();
  });

  let router = TestBed.get(Router); 
  location = TestBed.get(Location); 
  fixture = TestBed.createComponent(HeroesListComponent);
  router.initialNavigation();

  it('#component Deberia ser verdadero', () => {
    expect(component).toBeTruthy
  });


 it('HeroeService deberia Inyectarse correctamente', inject(    
 [HeroesService], (service: HeroesService) => {    
 expect(service).toBeTruthy();    
 }));    


//  it('navigate to "" redirects you to /heroe/ID', fakeAsync(() => { 
//   router.navigate(['/heroe/1']); 
//   tick(50); 
//    component.go_to(1)
//    expect(location.path().toBe('/heroe/1'));
//  }));

  

  
})


