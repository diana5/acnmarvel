import { Component, OnInit } from '@angular/core';
import { HeroesService } from 'src/app/services/heroes.service';
import { Heroe } from '../../classes/heroe';
import { FormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-heroes-list',
  templateUrl: './heroes-list.component.html',
  styleUrls: ['./heroes-list.component.css']
})
export class HeroesListComponent implements OnInit {
  totalPages: any;

  constructor(private heroesService: HeroesService, private router:Router) { }

  public heroes: any;
  public searchString : string;
  page = 0;
  step = 20;
  total = 0;
  title="Lista de Héroes By: Diana Sánchez";
  public hero:any

  teams = new Map();

  submitSearch() {
    this.heroesService.resetPager();
    this.showHeroes(this.searchString);
    console.log("se busco=>"+this.searchString)
  }
  
  prevPage() {
    this.showHeroes(this.searchString, this.page - 1);
  }
  
  nextPage() {
    this.showHeroes(this.searchString, this.page + 1);
  }
  
  go_to(id){
    this.router.navigateByUrl('/heroe/'+id);
  }
  
  showHeroes(nameStartsWith?: string, page?: number):Observable<Heroe[]>{  
    this.heroes = [];
    console.log("TEAMS");
    console.log(Array.from(this.teams));
    if (this.page || this.page === 0) {
      this.page = this.page;
    }
   this.heroesService.getHeroes(nameStartsWith,page).subscribe((data) => {
     
      this.total = Math.ceil(data.data.total / this.step);
      this.totalPages=data.data.total;
   
      data.data.results.forEach( result => {
      
          this.heroes.push(new Heroe(
            result.id,
            result.name,
            result.description,
            result.modified,
            result.thumbnail,
            result.resourceURI,
            this.getTeamColor(result.id)
          ));
        
        }
      );
      console.log(this.heroes)
  
    });
    return this.heroes

  }

  getTeamColor(id):string{
    if(this.teams.get(id)!=undefined){
      return this.teams.get(id);
    }
    else{
      return "";
    }
  }
  
  ngOnInit() {
    this.showHeroes();
    
    
}



}